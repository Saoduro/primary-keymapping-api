var jwt = context.getVariable("request.header.authorization");

var idtype = context.getVariable("request.queryparam.idtype");

var idvalue = context.getVariable("request.queryparam.idvalue");

var regex = new RegExp(/^[a-zA-Z0-9+]+([\._-][a-zA-Z0-9]+)*@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9]+){0,4}\.[a-zA-Z0-9]{1,4}$/);


const idtypes = ["EMAIL","CDG","CRS","CHECK_DIGIT_ACCOUNT_ID","SF_ACCOUNT_ID","SF_USER_ID","SF_CONTACT_ID","MYACCOUNT_ID","GIGYA" ];



if(idtype === null) {
    context.setVariable("errorMessage", "Please provide the valid idtype as it's mandatory field");
    context.setVariable("path", "/properties/note/properties/idtype/nonExisting");
    context.setVariable("isRequestValid", false);
}
else if(idtypes.indexOf(idtype)==-1) {
    context.setVariable("errorMessage", "Please provide the valid idtype.");
    context.setVariable("path", "/properties/note/properties/idtype/nonExisting");
    context.setVariable("isRequestValid", false);
}
else if(idvalue === null || idvalue === "" || idvalue.length == '0') {
    context.setVariable("errorMessage", "Please provide the valid idvalue as it's mandatory field");
    context.setVariable("path", "/properties/note/properties/idvalue/nonExisting");
    context.setVariable("isRequestValid", false);
}
else if(idtype == "EMAIL" && !regex.test(idvalue)) {
    context.setVariable("errorMessage", "Provided idvalue (" + idvalue+" ), is not valid email");
    context.setVariable("path", "/properties/note/properties/idtype/nonExisting");
    context.setVariable("isRequestValid", false);
} 
else if( jwt !=null && jwt.replace(/\s+/g, '') == '') {
    context.setVariable("errorMessage", "Please provide the JWT");
    context.setVariable("path", "/properties/note/properties/authorization/nonExisting");
    context.setVariable("isRequestValid", false);
}
